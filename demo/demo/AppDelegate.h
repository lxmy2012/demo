//
//  AppDelegate.h
//  demo
//
//  Created by zhangweibiao on 16/1/6.
//  Copyright © 2016年 lxmy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

