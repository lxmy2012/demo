//
//  ViewController.m
//  demo
//
//  Created by zhangweibiao on 16/1/6.
//  Copyright © 2016年 lxmy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    UILabel *lbl = [UILabel new];
    lbl.frame = CGRectMake(100, 100, 100, 20);
    lbl.text = @"demo";
    lbl.textColor = [UIColor grayColor];
    [self.view addSubview:lbl];
    
    // 我就是测试下git而已
    
    
}

@end
